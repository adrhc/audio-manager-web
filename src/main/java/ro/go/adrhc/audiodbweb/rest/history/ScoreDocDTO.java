package ro.go.adrhc.audiodbweb.rest.history;

import com.fasterxml.jackson.annotation.JsonCreator;
import org.apache.lucene.search.FieldDoc;

public class ScoreDocDTO extends FieldDoc {
	@JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
	public ScoreDocDTO(int doc, float score, Object[] fields, int shardIndex) {
		super(doc, score, fields, shardIndex);
	}
}

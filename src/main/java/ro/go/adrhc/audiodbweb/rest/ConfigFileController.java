package ro.go.adrhc.audiodbweb.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.cli.ParseException;
import org.springframework.web.bind.annotation.*;
import ro.go.adrhc.audiomanager.infrastructure.filesystem.config.ConfigFileRepository;
import ro.go.adrhc.audiomanager.infrastructure.filesystem.config.FileNameAndContent;
import ro.go.adrhc.audiomanager.managers.ConfigFileManager;

import java.io.IOException;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/api/config-file")
@RequiredArgsConstructor
@Slf4j
public class ConfigFileController {
	private final ConfigFileRepository configFileRepository;
	private final ConfigFileManager configFileManager;

	@PostMapping
	public void updateContent(
			@RequestBody FileNameAndContent nameAndContent) throws IOException, ParseException {
		configFileManager.updateContent(nameAndContent);
	}

	@PostMapping("query")
	public Optional<FileNameAndContent> getByFileName(
			@RequestBody FileNameAndContent nameAndContent) throws IOException {
		return configFileRepository.getByFileName(nameAndContent.filename());
	}

	@GetMapping("names")
	public Set<String> getFileNames() {
		return configFileRepository.getFileNames();
	}
}

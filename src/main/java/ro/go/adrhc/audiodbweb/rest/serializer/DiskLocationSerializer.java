package ro.go.adrhc.audiodbweb.rest.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;

import java.io.IOException;

@Component
public class DiskLocationSerializer extends AbstractLocationSerializer<DiskLocation> {
	@Override
	protected void serializeObject(DiskLocation location, JsonGenerator jgen,
			SerializerProvider provider) throws IOException {
		super.serializeObject(location, jgen, provider);
		jgen.writeFieldName("name");
		jgen.writeString(location.name());
	}

	public Class<DiskLocation> handledType() {
		return DiskLocation.class;
	}
}

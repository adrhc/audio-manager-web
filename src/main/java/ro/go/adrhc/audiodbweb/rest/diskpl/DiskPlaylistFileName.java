package ro.go.adrhc.audiodbweb.rest.diskpl;

public record DiskPlaylistFileName(String fileName) {
}

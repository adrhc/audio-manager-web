package ro.go.adrhc.audiodbweb.rest.serializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.boot.jackson.JsonObjectDeserializer;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationType;

@Component
public class YtLocationDeserializer extends JsonObjectDeserializer<YouTubeLocation> {
	@Override
	protected YouTubeLocation deserializeObject(JsonParser jsonParser,
			DeserializationContext context, ObjectCodec codec, JsonNode tree) {
		String code = tree.get("code").asText();
		String ytType = tree.get("ytType").asText();
		return new YouTubeLocation(YouTubeLocationType.valueOf(ytType), code);
	}
}

package ro.go.adrhc.audiodbweb.rest.serializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.SneakyThrows;
import org.springframework.boot.jackson.JsonObjectDeserializer;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;

import java.net.URI;
import java.nio.file.Path;

import static ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory.createDiskLocation;

@Component
public class DiskLocationDeserializer extends JsonObjectDeserializer<DiskLocation> {
	@SneakyThrows
	@Override
	protected DiskLocation deserializeObject(
			JsonParser jsonParser, DeserializationContext context,
			ObjectCodec codec, JsonNode tree) {
		String rawPath = tree.get("uri").asText();
		URI uri = new URI(rawPath);
		return createDiskLocation(Path.of(uri));
	}
}

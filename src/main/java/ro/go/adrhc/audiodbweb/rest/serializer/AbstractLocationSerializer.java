package ro.go.adrhc.audiodbweb.rest.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.boot.jackson.JsonObjectSerializer;
import ro.go.adrhc.audiomanager.domain.location.Location;

import java.io.IOException;

public class AbstractLocationSerializer<L extends Location> extends JsonObjectSerializer<L> {
	@Override
	protected void serializeObject(L location, JsonGenerator jgen,
			SerializerProvider provider) throws IOException {
		jgen.writeFieldName("type");
		jgen.writeString(location.type().name());
		jgen.writeFieldName("uri");
		jgen.writeString(location.uri().toString());
	}
}

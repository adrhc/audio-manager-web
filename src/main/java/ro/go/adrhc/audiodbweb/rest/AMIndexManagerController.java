package ro.go.adrhc.audiodbweb.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ro.go.adrhc.audiomanager.managers.AMIndexManager;

import java.io.IOException;

@RestController
@RequestMapping("/api/index-manager")
@RequiredArgsConstructor
@Slf4j
public class AMIndexManagerController {
	private final AMIndexManager manager;

	@RequestMapping("shallowDiskUpdate")
	public Integer shallowDiskUpdate() throws IOException {
		log.info("\nuser asked to (shallow) sync the index with the disk entries");
		return manager.shallowDiskUpdate();
	}

	@RequestMapping("reset")
	public Integer reset() throws IOException {
		log.info("user asked to reset the index");
		return manager.reset();
	}
}

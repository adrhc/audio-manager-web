package ro.go.adrhc.audiodbweb.rest.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;

import java.io.IOException;

@Component
@RequiredArgsConstructor
public class YtLocationSerializer extends AbstractLocationSerializer<YouTubeLocation> {
	@Override
	public void serializeObject(YouTubeLocation location, JsonGenerator jgen,
			SerializerProvider provider) throws IOException {
		super.serializeObject(location, jgen, provider);
		jgen.writeFieldName("ytType");
		jgen.writeString(location.ytType().name());
		jgen.writeFieldName("code");
		jgen.writeString(location.code());
	}

	public Class<YouTubeLocation> handledType() {
		return YouTubeLocation.class;
	}
}

package ro.go.adrhc.audiodbweb.rest.serializer;

import com.fasterxml.jackson.databind.module.SimpleModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;

@Configuration
public class JacksonConfig {
	@Bean
	public SimpleModule customSerializers(
			DiskLocationDeserializer diskLocationDeserializer,
			DiskLocationSerializer diskLocationJsonSerializer,
			YtLocationDeserializer ytLocationDeserializer,
			YtLocationSerializer youTubeLocationSerializer,
			UriLocationSerializer uriLocationSerializer) {
		SimpleModule module = new SimpleModule();
		module.addSerializer(diskLocationJsonSerializer);
		module.addDeserializer(DiskLocation.class, diskLocationDeserializer);
		module.addDeserializer(YouTubeLocation.class, ytLocationDeserializer);
		module.addSerializer(youTubeLocationSerializer);
		module.addSerializer(uriLocationSerializer);
		return module;
	}
}

package ro.go.adrhc.audiodbweb.rest.diskpl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import ro.go.adrhc.audiomanager.datasources.disk.playlists.DiskPlaylistsRepository;
import ro.go.adrhc.audiomanager.managers.diskplquery.DiskPlQueryManager;
import ro.go.adrhc.audiomanager.managers.diskplquery.LocationSelections;
import ro.go.adrhc.audiomanager.managers.diskplquery.UriSelectedLocations;
import ro.go.adrhc.audiomanager.managers.diskplupdate.DiskPlUpdateManager;
import ro.go.adrhc.audiomanager.managers.diskplupdate.PlContentUpdateRequest;
import ro.go.adrhc.audiomanager.managers.diskplupdate.UriPlAllocationResult;

import java.io.IOException;

@RestController
@RequestMapping("/api/disk/playlist")
@RequiredArgsConstructor
@Slf4j
public class DiskPlaylistController {
	private final DiskPlQueryManager diskPlQueryManager;
	private final DiskPlUpdateManager diskPlUpdateManager;
	private final DiskPlaylistsRepository diskPlRepository;

	@GetMapping
	public LocationSelections findPlLocationsByEntry(
			@RequestParam("uri") String uri) throws IOException {
		return diskPlQueryManager.findPlLocationsByEntry(uri);
	}

	@PostMapping("add-song-to-playlists")
	public UriPlAllocationResult updateUriPlaylists(
			@RequestBody UriSelectedLocations uriSelectedLocations) {
		return diskPlUpdateManager.updateUriPlaylists(uriSelectedLocations);
	}

	@PostMapping
	public void updatePlContent(
			@RequestBody PlContentUpdateRequest plContentUpdateReq) {
		diskPlUpdateManager.updatePlContent(plContentUpdateReq).orElseThrow();
	}

	@DeleteMapping
	public boolean removeDiskPlaylist(@RequestBody DiskPlaylistFileName diskPlFileName) throws IOException {
		return diskPlRepository.remove(diskPlFileName.fileName());
	}
}

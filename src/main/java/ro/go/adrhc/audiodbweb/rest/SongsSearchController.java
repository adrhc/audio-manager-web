package ro.go.adrhc.audiodbweb.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;
import ro.go.adrhc.audiomanager.managers.SongsSearchManager;

import java.io.IOException;

@RestController
@RequestMapping("/api/songs-search")
@RequiredArgsConstructor
@Slf4j
public class SongsSearchController {
	private final SongsSearchManager songsSearchManager;

	@GetMapping
	public PlaylistEntries search(String text) throws IOException {
		return songsSearchManager.search(text);
	}

	@GetMapping("structured")
	public PlaylistEntries structuredSearch(String title,
			String artist, String text) throws IOException {
		return songsSearchManager.structuredSearch(title, artist, text);
	}

	@GetMapping("disk")
	public PlaylistEntries searchOnDisk(String title,
			String artist, String text) throws IOException {
		return songsSearchManager.searchOnDisk(title, artist, text);
	}

	@GetMapping("ytvideo")
	public PlaylistEntries searchYouTubeVideos(String title, String artist, String text) {
		return songsSearchManager.searchYouTubeVideos(title, artist, text);
	}

	@GetMapping("ytmusic")
	public PlaylistEntries searchYouTubeMusic(String title, String artist, String text) {
		return songsSearchManager.searchYouTubeMusic(title, artist, text);
	}
}

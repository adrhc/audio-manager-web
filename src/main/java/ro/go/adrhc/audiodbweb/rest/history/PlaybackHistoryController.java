package ro.go.adrhc.audiodbweb.rest.history;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import ro.go.adrhc.audiodbweb.domain.TrackWithImages;
import ro.go.adrhc.audiodbweb.manager.PlEntriesHistoryPage;
import ro.go.adrhc.audiodbweb.manager.PlaybackHistoryManager;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/history")
@RequiredArgsConstructor
@Slf4j
public class PlaybackHistoryController {

    private final PlaybackHistoryManager manager;

    @GetMapping
    public PlEntriesHistoryPage browse() throws IOException {
        return manager.getFirst();
    }

    @PostMapping("before")
    public PlEntriesHistoryPage browseBefore(@RequestBody ScoreDocDTO before) throws IOException {
        return manager.getPrevious(before);
    }

    @PostMapping("after")
    public PlEntriesHistoryPage browseAfter(@RequestBody ScoreDocDTO after) throws IOException {
        return manager.getNext(after);
    }

    @PostMapping
    public void addMany(@RequestBody List<TrackWithImages> trackWithImages) throws IOException {
        manager.addMany(trackWithImages);
    }
}

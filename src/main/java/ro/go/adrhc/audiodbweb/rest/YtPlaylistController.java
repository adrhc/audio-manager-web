package ro.go.adrhc.audiodbweb.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;
import ro.go.adrhc.audiomanager.managers.YtEntriesManager;

@RestController
@RequestMapping("/api/youtube/playlist")
@RequiredArgsConstructor
@Slf4j
public class YtPlaylistController {
	private final YtEntriesManager ytEntriesManager;

	@GetMapping
	public PlaylistEntries loadMusicPlaylists() {
		return ytEntriesManager.loadMusicPlaylists();
	}

	@GetMapping("{ytUri}")
	public PlaylistEntries loadFromPlaylist(@PathVariable("ytUri") String ytUri) {
		return ytEntriesManager.loadFromPlaylist(ytUri);
	}
}

package ro.go.adrhc.audiodbweb.rest.serializer;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.domain.location.UriLocation;

@Component
@RequiredArgsConstructor
public class UriLocationSerializer extends AbstractLocationSerializer<UriLocation> {
	@Override
	public Class<UriLocation> handledType() {
		return UriLocation.class;
	}
}

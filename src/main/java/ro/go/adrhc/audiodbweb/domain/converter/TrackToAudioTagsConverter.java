package ro.go.adrhc.audiodbweb.domain.converter;

import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiodbweb.domain.Artist;
import ro.go.adrhc.audiodbweb.domain.Track;
import ro.go.adrhc.lib.audiometadata.domain.AudioTags;

import java.util.Arrays;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class TrackToAudioTagsConverter implements Converter<Track, AudioTags> {
	@Override
	public AudioTags convert(@NonNull Track track) {
		AudioTags audioTags = new AudioTags();
		if (track.hasMusicbrainzId()) {
			audioTags.putTag("musicbrainzId", track.getMusicbrainzId());
		}
		if (track.hasComment()) {
			audioTags.putTag("comment", track.getComment());
		}
		if (track.hasAlbum()) {
			audioTags.putTag("album", track.getAlbum().getName());
		}
		if (track.hasAlbumArtists()) {
			audioTags.putTag("album-artists", Arrays.stream(track.getAlbum().getArtists())
					.map(Artist::getName).sorted().collect(Collectors.joining(" ")));
		}
		if (track.hasArtists()) {
			audioTags.putTag("artists", Arrays.stream(track.getArtists())
					.map(Artist::getName).sorted().collect(Collectors.joining(" ")));
		}
		if (track.hasComposers()) {
			audioTags.putTag("composers", Arrays.stream(track.getComposers())
					.map(Artist::getName).sorted().collect(Collectors.joining(" ")));
		}
		if (track.hasPerformers()) {
			audioTags.putTag("performers", Arrays.stream(track.getPerformers())
					.map(Artist::getName).sorted().collect(Collectors.joining(" ")));
		}
		return audioTags;
	}
}

package ro.go.adrhc.audiodbweb.domain;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import static org.springframework.util.StringUtils.hasText;

/*
 * See: https://github.com/mopidy/mopidy/blob/develop/mopidy/models/__init__.py
 */
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@Getter
@Setter
@ToString
public class Track {
	private String uri;
	private String name;
	private Artist[] artists;
	private Album album;
	private Artist[] composers;
	private Artist[] performers;
	private String genre;
	private Integer trackNo;
	private Integer discNo;
	private String date;            // Release date (YYYY or YYYY-MM-DD)
	private Long length;            // In milliseconds
	private Integer bitrate;        // In kBit/s
	private String comment;
	private String musicbrainzId;
	private Long lastModified;

	public boolean hasArtists() {
		return artists != null && artists.length > 0;
	}

	public boolean hasAlbumArtists() {
		return album != null && album.getArtists() != null && album.getArtists().length > 0;
	}

	public boolean hasComposers() {
		return composers != null && composers.length > 0;
	}

	public boolean hasPerformers() {
		return performers != null && performers.length > 0;
	}

	public boolean hasAlbum() {
		return album != null;
	}

	public boolean hasMusicbrainzId() {
		return hasText(musicbrainzId);
	}

	public boolean hasComment() {
		return hasText(comment);
	}
}
package ro.go.adrhc.audiodbweb.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/*
 * See: https://github.com/mopidy/mopidy/blob/develop/mopidy/models/__init__.py
 */
@Getter
@Setter
@ToString
public class Image {
	private String uri;
	private int width;
	private int height;
}
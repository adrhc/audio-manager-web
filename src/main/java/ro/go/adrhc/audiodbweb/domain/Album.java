package ro.go.adrhc.audiodbweb.domain;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/*
 * See: https://github.com/mopidy/mopidy/blob/develop/mopidy/models/__init__.py
 */
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@Getter
@Setter
@ToString
public class Album {
	private String uri;
	private String name;
	private Artist[] artists;
	private Integer numTracks;
	private Integer numDiscs;
	private String date;            // Release date (YYYY or YYYY-MM-DD)
	private String musicbrainzId;
}
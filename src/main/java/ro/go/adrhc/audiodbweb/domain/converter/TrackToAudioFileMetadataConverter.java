package ro.go.adrhc.audiodbweb.domain.converter;

import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiodbweb.domain.Track;
import ro.go.adrhc.audiodbweb.domain.TrackWithImages;
import ro.go.adrhc.audiomanager.domain.location.Location;
import ro.go.adrhc.audiomanager.domain.location.LocationParsers;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadataCollection;

import java.util.Collection;

import static org.springframework.util.StringUtils.hasText;
import static ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocationAccessors.path;
import static ro.go.adrhc.audiomanager.domain.location.LocationType.DISK;

@Component
@RequiredArgsConstructor
public class TrackToAudioFileMetadataConverter
		implements Converter<TrackWithImages, AudioMetadata> {
	private final LocationParsers locationParsers;
	private final TrackToAudioTagsConverter audioTagsConverter;

	public AudioMetadataCollection convertMany(
			@NonNull Collection<TrackWithImages> tracksWithImages) {
		return AudioMetadataCollection.of(tracksWithImages.stream().map(this::convert));
	}

	@Override
	public AudioMetadata convert(@NonNull TrackWithImages trackWithImages) {
		Location location = locationParsers.parse(trackWithImages.getUri());
		if (location.type() == DISK) {
			return AudioMetadata.of(path(location), location.type(),
					audioTagsConverter.convert(trackWithImages));
		} else {
			return AudioMetadata.of(location.uri(), location.type(),
					toFileNameNoExt(trackWithImages), audioTagsConverter.convert(trackWithImages));
		}
	}

	private String toFileNameNoExt(Track track) {
		if (hasText(track.getName())) {
//			String artists = getArtists(track);
//			return artists.isBlank() ? track.getName() : STR."\{track.getName()} - \{artists}";
			return track.getName();
		} else {
			return track.getUri();
		}
	}

	/*private static String getArtists(Track track) {
		return Stream.of(track.getArtists())
				.map(Artist::getName)
				.collect(Collectors.joining(" "));
	}*/
}

package ro.go.adrhc.audiodbweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Import;
import ro.go.adrhc.audiomanager.AudioManagerConfig;

@EnableConfigurationProperties
@SpringBootApplication
@EnableFeignClients
@Import(AudioManagerConfig.class)
public class AudioManagerWebApplication {
	public static void main(String[] args) {
		SpringApplication.run(AudioManagerWebApplication.class, args);
	}
}

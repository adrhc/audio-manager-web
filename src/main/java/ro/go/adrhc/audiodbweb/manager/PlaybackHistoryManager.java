package ro.go.adrhc.audiodbweb.manager;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.search.ScoreDoc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ro.go.adrhc.audiodbweb.domain.Track;
import ro.go.adrhc.audiodbweb.domain.TrackWithImages;
import ro.go.adrhc.audiodbweb.domain.converter.TrackToAudioFileMetadataConverter;
import ro.go.adrhc.audiomanager.datasources.audiofiles.history.AudioMediaHistory;
import ro.go.adrhc.audiomanager.datasources.audiofiles.history.HistoryPage;
import ro.go.adrhc.audiomanager.datasources.audiofiles.metadata.AudioMetadataToPlEntryConverter;
import ro.go.adrhc.audiomanager.domain.location.LocationCollection;
import ro.go.adrhc.audiomanager.domain.location.LocationParsers;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadataCollection;
import ro.go.adrhc.persistence.lucene.FileSystemIndex;

import java.io.IOException;
import java.net.URI;
import java.util.Collection;
import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class PlaybackHistoryManager {

    private final AudioMetadataToPlEntryConverter toPlEntryConverter;

    private final FileSystemIndex<URI, AudioMetadata> amIndexRepository;

    private final LocationParsers locationParsers;

    private final TrackToAudioFileMetadataConverter converter;

    private final AudioMediaHistory history;

    @Value("${history.page-size}")
    private final int completePageSize;

    public PlEntriesHistoryPage getFirst() throws IOException {
        return toPlEntriesHistoryPage(history.getFirst());
    }

    public PlEntriesHistoryPage getPrevious(ScoreDoc before) throws IOException {
        return toPlEntriesHistoryPage(history.getPrevious(before));
    }

    public PlEntriesHistoryPage getNext(ScoreDoc after) throws IOException {
        return toPlEntriesHistoryPage(history.getNext(after));
    }

    public void addMany(List<TrackWithImages> tracks) throws IOException {
        List<String> uris = toUris(tracks);
        LocationCollection locations = locationParsers.parseMany(uris).known();
        if (locations.isEmpty()) {
            log.error("\nCan register the tracks because they all have unknown location!");
            return;
        } else if (locations.size() != tracks.size()) {
            log.error("\nCan't register {} unknown locations!", tracks.size() - locations.size());
        }
        AudioMetadataCollection metadata = converter.convertMany(tracks);
        if (metadata.size() == 1) {
            log.debug("\nTrack to add to the history:\n{}\nAdding to the history:\n{}",
                    tracks.getFirst(), metadata.getFirst());
            amIndexRepository.merge(metadata.getFirst(), AudioMetadata::merge);
        } else {
            log.debug("Adding {} tracks to the history!", tracks.size());
            amIndexRepository.mergeMany(metadata.getMetadata(), AudioMetadata::merge);
        }
    }

    /*public void add(TrackWithImages trackWithImages) throws IOException {
        Location location = locationParsers.parse(trackWithImages.getUri());
        if (location.type() == UNKNOWN) {
            log.error("\nCan't register:\n{}", trackWithImages);
        } else {
            log.debug("The track to add to the history:\n{}", trackWithImages);
            AudioMetadata metadata = converter.convert(trackWithImages);
            log.debug("\nAdding to history:\n{}", metadata);
            amIndexRepository.merge(metadata);
        }
    }*/

    private PlEntriesHistoryPage toPlEntriesHistoryPage(HistoryPage<AudioMetadata> page) {
        List<PlaylistEntry> entries = page.rawFlatMap(toPlEntryConverter::convert).toList();
        return PlEntriesHistoryPage.of(entries, page, completePageSize);
    }

    private List<String> toUris(Collection<TrackWithImages> tracks) {
        return tracks.stream().map(Track::getUri).toList();
    }
}

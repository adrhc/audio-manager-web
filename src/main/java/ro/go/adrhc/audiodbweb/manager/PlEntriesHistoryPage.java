package ro.go.adrhc.audiodbweb.manager;

import org.apache.lucene.search.ScoreDoc;
import ro.go.adrhc.audiomanager.datasources.audiofiles.history.HistoryPage;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;

import java.util.List;

public record PlEntriesHistoryPage(List<PlaylistEntry> entries, ScoreDoc first,
		ScoreDoc last, boolean pageBeforeExists, boolean pageAfterExists, int completePageSize) {
	public static PlEntriesHistoryPage of(List<PlaylistEntry> entries,
			HistoryPage<AudioMetadata> page, int completePageSize) {
		return new PlEntriesHistoryPage(entries, page.firstPosition(),
				page.lastPosition(), page.pageBeforeExists(), page.pageAfterExists(),
				completePageSize);
	}
}

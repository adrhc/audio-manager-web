package ro.go.adrhc.audiodbweb.manager;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@EnableConfigurationProperties
@SpringBootTest(properties = {"history.page-size=5",
		"lucene.search.result-includes-missing-files=true"})
@DirtiesContext
@Slf4j
class PlaybackHistoryManagerIT {
	/*@TempDir static Path tmpDir;*/
	@Autowired
	private PlaybackHistoryManager manager;
	@Value("${history.page-size}")
	private int historyPageSize;

	@Test
	void getFirst() throws IOException {
		PlEntriesHistoryPage page = manager.getFirst();
		assertThat(page.entries()).hasSize(historyPageSize);
		assertThat(page.pageBeforeExists()).isFalse();
		assertThat(page.pageAfterExists()).isTrue();
	}

	/*static class IndexPathInitializer
			implements ApplicationContextInitializer<ConfigurableApplicationContext> {
		@Override
		public void initialize(@NonNull ConfigurableApplicationContext context) {
			TestPropertyValues.of(STR."context-paths.index-path=\{tmpDir}",
					STR."context-paths.songs-path=\{tmpDir}").applyTo(context);
		}
	}*/
}
package ro.go.adrhc.audiodbweb.manager;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.OS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;
import ro.go.adrhc.audiomanager.domain.location.Location;

import static org.assertj.core.api.Assertions.assertThat;
import static ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory.createDiskLocation;
import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationFactory.ytMusicLocation;

@EnableConfigurationProperties
@SpringBootTest(properties = {
		"lucene.read-only=true", "history.page-size=5",
		"lucene.search.result-includes-missing-files=true"})
@Slf4j
class LocationSerializerTest {
	private static final String WIN_LOCATION_JSON =
			"{\"type\":\"DISK\",\"uri\":\"file:///C:/test%20playlist.m3u8\",\"name\":\"test playlist\"}";
	private static final String LINUX_LOCATION_JSON =
			"{\"type\":\"DISK\",\"uri\":\"file:///test%20playlist.m3u8\",\"name\":\"test playlist\"}";
	private static final String YT_LOCATION_JSON =
			"{\"type\":\"YOUTUBE\",\"uri\":\"ytmusic:track:ytcode\",\"ytType\":\"MUSIC\",\"code\":\"ytcode\"}";
	@Autowired
	private ObjectMapper mapper;

	@Test
	void serializeDiskLocation() throws JsonProcessingException {
		Location location = createDiskLocation(path());
		String json = mapper.writeValueAsString(location);
		assertThat(json).isEqualTo(diskJson());
	}

	@Test
	void deserializeDiskLocation() throws JsonProcessingException {
		DiskLocation location = mapper.readValue(diskJson(), DiskLocation.class);
		assertThat(location.name()).isEqualTo("test playlist");
		assertThat(location.rawPath()).isEqualTo(path());
	}

	@Test
	void serializeYtLocation() throws JsonProcessingException {
		Location location = ytMusicLocation("ytcode");
		String json = mapper.writeValueAsString(location);
		assertThat(json).isEqualTo(YT_LOCATION_JSON);
	}

	@Test
	void deserializeYtLocation() throws JsonProcessingException {
		YouTubeLocation location = mapper.readValue(YT_LOCATION_JSON, YouTubeLocation.class);
		assertThat(location.mediaId()).isEqualTo("ytmusic:track:ytcode");
	}

	private static String diskJson() {
		return OS.WINDOWS.isCurrentOs() ? WIN_LOCATION_JSON : LINUX_LOCATION_JSON;
	}

	private static String path() {
		return OS.WINDOWS.isCurrentOs() ? "C:\\test playlist.m3u8" : "/test playlist.m3u8";
	}
}

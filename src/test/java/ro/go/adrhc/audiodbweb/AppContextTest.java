package ro.go.adrhc.audiodbweb;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class AppContextTest {
	@Test
	void contextLoads() {
		log.info("context loaded");
	}
}
